/*
Лента сообщений и форма для сообщений

Нужно написать свой прогрмамму которая слева выводит список сообщений,
а справа находится форма для добавления этих сообщений.

Каждое сообщение можно:
+   Показать полностью (если больше 500 знаков)
+   Лайкнуть (с подсчетом лайков)
+   Показать все коментарии
+   Написать комментарий
*/

// Done
// +++ кликабельное количество комментариев - скрывать/показывать
// +++ превью изображения, если указан УРЛ
// +++ превью изображения, если выбирали локальный, УРЛ очистить
// +++ при сохранении, сохранять путь/УРЛ файла
// +++ сделать красивый вывод даты в сообщениях
// +++ до 500 автоматически высчитывать высоту
// +++ больше 500 часть скрывать, и показывать по клику (фокусу)

//
class MyPost {
  constructor(guid, author, text, image, date){
    this.guid = guid;
    this.author = author;
    this.text = text;
    this.image = image;
    this.date = date;
    this.likes = 0;
    this.comments = [];
  }

  savePost(){
    if(this.guid){
      var cnt = this.text.length;
      localStorage.setItem(this.guid, JSON.stringify(this));
      //alert("Post saved");
    }
  };

  addLike(guid){
    let tmpData = JSON.parse(localStorage.getItem(guid));
    tmpData.likes++;
    localStorage.setItem(guid, JSON.stringify(tmpData));
    //alert("Post liked");
  };
  
  sendComment(guid, author, comment){
    if(author&&comment){
      let tmpData = JSON.parse(localStorage.getItem(guid));
      let commDate = new Date();
      let newComment = new MyComment(commDate, author, comment);
      tmpData.comments.push(newComment);
      localStorage.setItem(guid, JSON.stringify(tmpData));
      //alert("Comment saved");
    }else{
      alert("Warning! Must be input Author and Text");
    }
  };
}  // class 

//
class MyComment {
  constructor(date, author, comment){
    this.date = date;
    this.author = author;
    this.comment = comment;
  }
}

//
addLike = function( event ){
  let currentPost = new MyPost;
  currentPost.guid = event.target.parentNode.parentNode.children[1].children[0].textContent;
  currentPost.addLike(currentPost.guid);
  fillNewsFeed();
}

//
sendComment = function( event ){
  let vGuid = event.target.parentNode.parentNode.parentNode.childNodes[1].childNodes[3].childNodes[1].textContent;
  let vAuthor  = event.target.parentNode.parentNode.childNodes[1].childNodes[1].value; 
  let vComment = event.target.parentNode.parentNode.childNodes[3].childNodes[1].value;
  let currentPost = new MyPost;
  currentPost.sendComment(vGuid, vAuthor, vComment);
  fillNewsFeed();
}

//
addComment = function( event ){
  let arrTmpNodes = event.target.parentNode.parentNode.parentNode.children;
  let tmpNode = arrTmpNodes[arrTmpNodes.length-1];
  tmpNode.classList.toggle("my-class-nodisplay");
}

show_hide_comments = function( event ){
//  alert("show_hide_comments");
    let arrTmpNodes = Array.from( event.target.parentNode.parentNode.parentNode.children );

  arrTmpNodes.forEach(function( item ){
    if(item.id === "commentBlock"){
      item.classList.toggle("my-class-nodisplay");
    }
  })
}

//
loadFile = function(event){
  //alert("load File");
  var input = document.createElement("input");
      input.type = "file";
      input.addEventListener("change", handleFile)
      input.click();
}

handleFile = function(event){
  //alert("handleFiles");
  var input = event.path[0];

  if(input.files[0]){
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event){
      var img = document.getElementById("postImage");
          img.src = event.target.result;
    } 
  }
}

//
onURLChange = function(){
//  alert("onURLChange");
  var url = document.getElementById("pathImage").value;

  if (url){
    var img = document.getElementById("postImage");
        img.src = url;
  }
}

//
sendNewPost = function(){
  let newGuid = getGuid();
  let postDate = new Date();
  let elemPostAuthor = document.getElementById("postAuthor");
  let elemPostText = document.getElementById("postTextArea");
  let elemPostImage = document.getElementById("postImage");
  let elemPathImage = document.getElementById("pathImage");

  if(elemPostAuthor.value && elemPostText.value){
    let newPost = new MyPost(
      newGuid, elemPostAuthor.value, elemPostText.value, elemPostImage.src, postDate);

    newPost.savePost();

    elemPostAuthor.value = "";
    elemPostText.value="";
    elemPostImage.src="images/noimage.png";
    elemPathImage.value="";

    fillNewsFeed();
    } else {
      alert("Warning! Must be input Author and Post text");
    }
}

//
getGuid = function(){
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}

//
onAppClick = function ( e ) {
  e.preventDefault();
  if(e.target.nodeName === "BUTTON") {
    switch(e.path[0].className) {
      case "my-button-send-post":  //send new post
        sendNewPost();
        break;
      case "my-button-load":  //load file
        loadFile( e );
        break;
      case "my-button-add-comment":  //add comment
        addComment( e );
        break;
      case "my-button-send-comment":  //send comment
        sendComment( e );
        break;
      case "my-button-likes":  //add like
        addLike( e );
        break;
    }  // switch(e.path..)
  } else if(e.target.id === "count_comment_text"){
    show_hide_comments( e );
  } else if(e.target.className === "my-textarea-message-comment") {
    // resize comment
    var targetScrollHeight = e.target.scrollHeight - 7;
    e.target.style.height = targetScrollHeight +'px';
    var tmpLabel = e.target.parentNode.children["1"];
        tmpLabel.innerText = "";
  } else if(e.target.className === "my-label-font-12"){
    // resize message
    var textArea = e.target.parentNode.children["0"];
        textArea.style.height = textArea.scrollHeight +'px';
    e.target.innerText = "";
  }
}  // AppClick

//
fillNewsFeed = function(){
  // отсортировать по дате
  function compare(a, b) {
    if(a.date && b.date){
      if (a.date < b.date) {
        return -1;
      }
      if (a.date > b.date) {
        return 1;
      }
      return 0;
    } else {
      objA = JSON.parse(localStorage.getItem(a));
      objB = JSON.parse(localStorage.getItem(b));
      if (objA.date < objB.date) {
        return -1;
      }
      if (objA.date > objB.date) {
        return 1;
      }
      return 0;
    }
  }

  //
  let arrGuidList = Object.keys(localStorage);
  arrGuidList.sort(compare);

  //
  let elemBlockNewsFeed = document.getElementById("blockNewsFeed");
  let tmpHTML = "<H1>News Feed</H1>";

  // 
  arrGuidList.forEach(function(item){  // Object.keys(localStorage)
    var returnObj = JSON.parse(localStorage.getItem(item));
    //console.log(returnObj);

    var d = new Date(returnObj.date),
    dformat = [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/')+' '+
              [d.getHours(), d.getMinutes()].join(':');

    //
tmpHTML = tmpHTML + `
<div class="my-flex-block-tools-comment">
  <div style="width:50%;"><label><b>` + returnObj.author + `</b></label></div>
  <div style="width:50%;text-align:right;"><label>` + dformat + `</label></div>
</div>
<div style="width:100%;"><img width=100% height=100% src="` + returnObj.image + `" style="align-content:center;"></div>
<div><textarea readonly class="my-textarea-message">` + returnObj.text + `</textarea>
<label class="my-label-font-12">.</label></div>

<div id="xblock-comments" class="my-flex-block-comments">
  <div class="my-flex-block-tools-comment">
    <div class="my-flex-block-tool-comment">
      <button class="my-button-likes"><img width=20px height=20px src="images/like_1.png"/>Likes ` + returnObj.likes + `</button>
    </div>
    <div class="my-flex-block-tool-comment" style="display:none;">
      <label>` + returnObj.guid + `</label>
    </div>
    <div class="my-flex-block-tool-comment" style="text-align:right;">
      <label id="count_comment_text">Comments(` + returnObj.comments.length + `)</label>
    </div>
    <div class="my-flex-block-tool-comment" style="text-align:right;">
      <button onclick="addComment" class="my-button-add-comment">Write comment</button>
    </div> <!-- end button-add-comment -->
  </div> <!-- end block-tools-comment -->`;

  // отсортировать по дате
  let arrCommentList = returnObj.comments;
      arrCommentList.sort(compare);
  
  //
  arrCommentList.forEach(function(itemComment){  // returnObj.comments
  //  console.log(itemComment.date, itemComment.author, itemComment.comment);
  
  d = new Date(itemComment.date),
  dformat = [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/')+' '+
            [d.getHours(), d.getMinutes()].join(':');

  //
  tmpHTML = tmpHTML + `
  <div id = "commentBlock" class="my-flex-block-comments my-class-nodisplay">
    <div class="my-flex-block-tools-comment">
      <div style="width:50%;">
        <label><b>` + itemComment.author + `</b></label>
      </div>
      <div style="width:50%;text-align:right;">
        <label>` + dformat + `</label>
      </div>
    </div>
    <div>
      <textarea readonly class="my-textarea-message-comment">` + itemComment.comment + `</textarea>
      <label class="my-label-font-10">.</label>
    </div>
  </div> <!-- end block-block-comments -->`;
  });  // arrCommentList.forEach

  //
  tmpHTML = tmpHTML + `
  <div class="my-flex-block-add-comment my-class-nodisplay">
    <div>
      <input placeholder=Author style="float:left;width:98%;margin:3px;"></input>
    </div>
    <div>
      <textarea placeholder=Comment style="width:98%;resize:none;margin:3px;"></textarea>
    </div>
    <div>
      <button style="width:99%;" class="my-button-send-comment">Send comment</button>
    </div>
  </div> <!-- end my-flex-block-add-comment" -->
</div> <!-- end id="xblock-comments" -->`;

  })  // keys(localStorage).forEach

  //
  elemBlockNewsFeed.innerHTML = tmpHTML;

  //
  resize = function( textArea ){
    var textAreaHeight = textArea.scrollHeight + 10;
        textArea.style.height = textAreaHeight +'px';
  }

  let arrElementMessage = Array.from( document.getElementsByClassName("my-textarea-message") );
  arrElementMessage.forEach(function(item){
    if(item.textLength >= 500){
      var tmpText = item.innerHTML;
      var tmpLabel = item.parentNode.children["1"];
          tmpLabel.innerText = "More...";
      item.innerHTML = "";
      for(var i=1; i<=500; i++){
        item.innerHTML = item.innerHTML + "x";
      }
      resize(item);
      item.innerHTML = tmpText;
    } else {
      resize(item);
    }
  })

  let arrElementComment = Array.from( document.getElementsByClassName("my-textarea-message-comment") );
  arrElementComment.forEach(function(item){
    if(item.textLength >= 50){
      var tmpLabel = item.parentNode.children["1"];
          tmpLabel.innerText = "Click comment for more";
      resize(item);
    } else {
      resize(item);
    }
  })

}  // fillNewsFeed = function()

//
window.addEventListener("load", function ( e ) {
  e.preventDefault();

  let elApp = document.getElementsByClassName("my-flex-container");
      elApp[0].addEventListener("click", onAppClick);

  let pathImage = document.getElementById("pathImage");
      pathImage.addEventListener("change", onURLChange);

  fillNewsFeed();
})


// https://html5.by/blog/flexbox/
// https://tproger.ru/articles/localstorage/
// https://stackoverflow.com/questions/19183180/how-to-save-an-image-to-localstorage-and-display-it-on-the-next-page/19183658
// https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value
// https://stackoverflow.com/questions/4851595/how-to-resolve-the-c-fakepath
// https://stackoverflow.com/questions/934012/get-image-data-in-javascript
// 
