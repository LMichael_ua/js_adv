// Задание 0:

// - Вывести ОДИН лог в котором будет идентефикатор 'Combined log:' и массив Table
console.log('Hi console:', Table);

// - Вывести в консоль табличку из массива который находится в переменной Table
console.log({Table});

// - Вывести сообщение с ошибкой "Oops! Something gonna wrong!"
console.error('Oops! Something gonna wrong!');

// - Вывести предупреждение "Be careful in the dark! It's can be dangerous"
console.warn('Be careful in the dark! It s can be dangerous');

// - Вывести цветной лог, где сообщение будет написано зеленым цветом и 30 размером шрифта
console.log('%cColor LOG 30px', "font-size:30px; color: green")

// - Сгрупировать несколько консоль логов, которые будут выводится друг за другом.
//     - log 1
//     - Group
//     - Log 2
console.group();
  console.log("log 1");
  console.log("Group");
  console.log("Log 2");
console.groupEnd();