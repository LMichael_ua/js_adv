  /*
      Задание:

      Сложить в элементе с id App следующую размету HTML:

      <header>
        <a href="http://google.com.ua">
          <img src="https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
        </a>
        <div class="menu">
          <a href="#1"> link 1</a>
          <a href="#1"> link 2</a>
          <a href="#1"> link 3</a>
        </div>
      </header>

      Используя следующие методы для работы:
      getElementById
      createElement
      element.innerText
      element.className
      element.setAttribute
      element.appendChild
  */

 var parentElement = document.getElementById('app');
 console.log(parentElement);

 var elHeader = document.createElement('header');

 var elHref = document.createElement('a');
 elHref.href = "http://google.com.ua";

 var elHrefImg = document.createElement('img');
 elHrefImg.src = "https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";

 var elDivClassMenu = document.createElement('div');
 elDivClassMenu.className = "menu";

 var elMenuHref1 = document.createElement('a');
 elMenuHref1.href = "#1"
 elMenuHref1.innerText = 'link 1';

 var elMenuHref2 = document.createElement('a');
 elMenuHref2.href = "#1"
 elMenuHref2.innerText = 'link 2';

 var elMenuHref3 = document.createElement('a');
 elMenuHref3.href = "#1"
 elMenuHref3.innerText = 'link 3';

 parentElement.appendChild(elHeader);
 elHeader.appendChild(elHref);
 elHref.appendChild(elHrefImg);
 elHeader.appendChild(elDivClassMenu);
 elDivClassMenu.appendChild(elMenuHref1);
 elDivClassMenu.appendChild(elMenuHref2);
 elDivClassMenu.appendChild(elMenuHref3);