// Задание:

// 1. Выбрать как элемент блок с id - test;
    console.warn('1. Выбрать как элемент блок с id - test');
    var idTest = document.getElementById('test');
    console.log('= id test:', idTest);

// 2. Выбрать по css селектору все button
    console.warn('2. Выбрать по css селектору все button');
    var getByCssAllButton = document.querySelectorAll('#app button');
    console.log('= list button by css:', getByCssAllButton);

// 3. Выбрать родительский элемент .wrap используя точку отсчета блок с с id = createArea.
    console.warn('3. Выбрать родительский элемент .wrap используя точку отсчета блок с с id = createArea');
    var idCreateArea = document.getElementById('createArea');
    console.log('= parent:', idCreateArea.closest('.wrap'));

// 4. Выбрать все элементы li по тегу
    console.warn('4. Выбрать все элементы li по тегу');
    var getByTagLi = document.getElementsByTagName('li');
    var arrByTagLi = Array.from(getByTagLi);
    console.log('= list by tag li:', arrByTagLi);

// 5. Выбрать все элементы по классу 'test'
    console.warn('5. Выбрать все элементы по классу "test"');
    var getByClassTest = document.getElementsByClassName('test');
    arrByClassTest = Array.from(getByClassTest);
    console.log('= list by class test:', arrByClassTest);
