/* Lanovenko M.

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

/*===========================================
Генерит случайный цвет в формате rgb(r,g,b)
---------------------------------------------*/
function getRandomColorRGB(){
  var vR=Math.floor(Math.random()*(255-0+1))+0;
  var vG=Math.floor(Math.random()*(255-0+1))+0;
  var vB=Math.floor(Math.random()*(255-0+1))+0;
  return "rgb("+vR+","+vG+","+vB+")";
}

/*===========================================
  Генерит случайный цвет в формате #fff
---------------------------------------------*/
function getRandomColorHEX(){
  var vR=(Math.floor(Math.random()*(255-15+1))+15).toString(16);
  var vG=(Math.floor(Math.random()*(255-15+1))+15).toString(16);
  var vB=(Math.floor(Math.random()*(255-15+1))+15).toString(16);
  return "#"+vR+vG+vB;
}

/*===========================================
  Устанавливает цвет фона body, 
  возврашает сообщение о коде цвета
---------------------------------------------*/
function setBackGroundColor(){
  vRandomColor1=getRandomColorHEX();
  document.body.style.background=vRandomColor1;
  return vRandomColor1;
}

/*===========================================
  Устанавливает цвет фона Id = app, 
  возврашает сообщение о коде цвета
---------------------------------------------*/
function setAppBgColor(){
  vRandomColor2=getRandomColorRGB();
  document.getElementById("app").style.background=vRandomColor2;
  return vRandomColor2;
}

/*===========================================
  Обработчик нажания кнопки id = button1
---------------------------------------------*/
var message1=document.createElement('div');
message1.setAttribute("id", "mess1");
var message2=document.createElement('div');
message2.setAttribute("id", "mess2");

function buttonClick(){

  if(document.getElementById("mess1")){document.body.removeChild(message1);}
  if(document.getElementById("mess2")){document.body.removeChild(message2);}
  
  var vBackGround = setBackGroundColor();
  var vAppBgColor = setAppBgColor();
  
  if(document.getElementById("checkBox1").checked){
    document.getElementById("messText1").innerText="BackGround:"+vBackGround;
    document.getElementById("messText2").innerText="AppBgColor:"+vAppBgColor;
  }else{
    document.getElementById("messText1").innerText="";
    document.getElementById("messText2").innerText="";
  }
  
  var scrWidth=window.innerWidth/2-50;
  var scrHeight=window.innerHeight/2;
  message1.style.cssText="position:fixed;width:100px;background-color:"+invertColor(vBackGround)+";text-align:center;border:2px solid gray;color:"+vBackGround;
  message1.style.left=scrWidth+"px";
  message1.style.top=scrHeight+"px";
  message1.innerText=vBackGround;
  document.body.appendChild(message1);

  var vApp=document.getElementById("app");
  var appWidth=vApp.offsetWidth/2-75+vApp.offsetLeft;
  var appHeight=vApp.offsetHeight/2-10+vApp.offsetTop;
  message2.style.cssText="position:fixed;width:150px;text-align:center;border:2px solid gray;color:black";
  message2.style.left=appWidth+"px";
  message2.style.top=appHeight+"px";
  message2.innerText=vAppBgColor;
  document.body.appendChild(message2);

}

/*===========================================
  Обработчик флажка id = checkBox1
---------------------------------------------*/
function checkBoxClick() {
/*
  if(document.getElementById("checkBox1").checked){
    document.getElementById("cbLabel").innerText = "Hide color code";
  }else{document.getElementById("cbLabel").innerText = "Show color code";}
*/
}

// CopyPaste
function invertColor(hex){
  if(hex.indexOf('#')===0){hex=hex.slice(1);}
  if(hex.length===3){hex=hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];}
  if(hex.length!==6){throw new Error('Invalid HEX color.');}
  var r=(255-parseInt(hex.slice(0,2),16)).toString(16),
      g=(255-parseInt(hex.slice(2,4),16)).toString(16),
      b=(255-parseInt(hex.slice(4,6),16)).toString(16);
  return '#'+padZero(r)+padZero(g)+padZero(b);
}
// CopyPaste
function padZero(str,len){len=len||2;var zeros=new Array(len).join('0');return(zeros + str).slice(-len);}

/*===========================================
  Объявление элементов
---------------------------------------------*/
// 
var parentElement = document.getElementById('app');
var divNote = document.getElementById('note');

// 
var elDiv1 = document.createElement('div');
var elDiv2 = document.createElement('div');
var elDiv3 = document.createElement('div');
var elDiv4 = document.createElement('div');

// объявление кнопки
var elButton = document.createElement('button');
elButton.innerText = "Press for change colors.";
elButton.setAttribute("id", "button1");
elButton.setAttribute("onClick", "buttonClick()");

// объявление текста для флажка
var elCbLabel = document.createElement('label');
elCbLabel.setAttribute("id", "cbLabel");
elCbLabel.innerText = "Show color code";

// объявление флажка
var elCheckBox = document.createElement('input');
elCheckBox.setAttribute("type", "checkbox");
elCheckBox.setAttribute("id", "checkBox1");
elCheckBox.setAttribute("onclick", "checkBoxClick()");

// объявление первого текста
var elMessText1 = document.createElement('label');
elMessText1.setAttribute("id", "messText1");
elMessText1.innerText = "";

// объявление второго текста
var elMessText2 = document.createElement('label');
elMessText2.setAttribute("id", "messText2");
elMessText2.innerText = "";

/*===========================================
  Добавление элементов
---------------------------------------------*/
parentElement.appendChild(elDiv1);
elDiv1.appendChild(elButton);

parentElement.appendChild(elDiv2);
elDiv2.appendChild(elCbLabel);
elCbLabel.appendChild(elCheckBox);

parentElement.appendChild(elDiv3);
elDiv3.appendChild(elMessText1);

parentElement.appendChild(elDiv4);
//document.body.appendChild(elDiv4);
elDiv4.appendChild(elMessText2);

/*===========================================
  Выполнение после обновления формы
---------------------------------------------*/
setBackGroundColor();
setAppBgColor();
