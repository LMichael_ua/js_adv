
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

  var header1 = document.getElementById("buttonContainer");
  //console.log(header);

  var btn1 = header1.getAttributeNode("button");
  console.log(btn1);

  var arrBtn = Array.from(document.getElementsByTagName("button"));
  //console.log(arrBtn);

  arrBtn.forEach(element => {
    console.log(element);
    var elementAttr = element.getAttribute("data-tab");
    console.log(elementAttr);
  });

  //
  var arrDivCont = Array.from(document.getElementsByTagName("div"));
  console.log(arrDivCont);

  //
  setTabActive = function(tabNum){
    arrDivCont.forEach(element => {
      var elementAttrClass = element.getAttribute("class");
      var elementAttrDatTab = element.getAttribute("data-tab");
      if(elementAttrClass=="tab"&&elementAttrDatTab==tabNum){
        element.classList.add("active");
        console.log("add active:", element, elementAttrDatTab);
      }
    });
  }

  //
  hideAllTabs = function(){
    arrDivCont.forEach(element => {
      var elementAttrClass = element.getAttribute("class");
      var elementAttrDatTab = element.getAttribute("data-tab");
      if(elementAttrClass!="tabContainer"){
        element.classList.remove("active");
        console.log("remove active:", element, elementAttrDatTab);
      }
    });
  }

//  setTabActive(1);
//  setTabActive(3);
//  hideAllTabs();

  header1.onclick = function(event) {
    // do stuff ...
    var tabNum = event.target.dataset.tab;
    if(tabNum){setTabActive(tabNum);}else{hideAllTabs();}
  }
