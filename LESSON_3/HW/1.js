/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;

  var elControl=document.getElementById("SliderControls");
  elControl.onclick=clickControl;

  var divSlider=document.getElementById("slider");
  var lenArr=OurSliderImages.length;

  var elemImg=new Image(); //64,48
  elemImg.classList.add('imgAnimated');
  elemImg.addEventListener('transitionend', transitionAnimation);

  var operation="none";  // с картинкой ничего не делать

  // обработчик анимации
  function transitionAnimation(e){
    switch(operation){
      case "max":  // если картинку нужно увеличить:
        setTimeout( function(){
          elemImg.classList.add("animation");  // увеличиваем
          operation="stop";  // и потом ничего с ней не делать 
        }, 1000);
        break;
      case "min":  // если картинку уменьшили
        RenderImage(OurSliderImages[currentPosition]);  // прорисуем новую картинку
        operation="max";  // и указываем, что нужно увеличить
        break;
      case "none": break; // ничего не делаем
    }
  }
 
  // при загрузке окна,
  onLoadWindow=function(){
    // прорисуем картинку, указывая ее ссылку из массива по значению счетчика
    RenderImage(OurSliderImages[currentPosition]); 
  }

  //
  //window.onload = onLoadWindow();
  //window.onload = onLoadWindow;
  //window.addEventListener('load', onLoadWindow());
  window.addEventListener('load', onLoadWindow);

  // прорисовка картинки по ссылке
  RenderImage=function(source){
    // если есть, удаляем elemImg
    if(divSlider.children.length!=0){divSlider.removeChild(elemImg);}
    // добавляем elemImg
    divSlider.appendChild(elemImg);
    // прорисовываем картинку
    //console.log(lenArr, currentPosition, source);
    elemImg.setAttribute("src", source);
  }

  // после нажания кнопок для смены картинки
  beforeChangeImage=function(){
    elemImg.classList.remove("animation");  // картинка уменьшается
    operation="min";  // и отмечаем, что уменьшили
  }

  // обрабка нажания кнопки
  NextSlide=function(){
    // увеличиваем счетчик, если нужно, корректируем его и запускаем смену картики
    currentPosition++;
    if(currentPosition>lenArr-1){currentPosition=0;}
    beforeChangeImage();
  }

  // обрабка нажания кнопки
  PrevSlide=function(){
    // уменьшаем счетчик, если нужно, корректируем его и запускаем смену картики
    currentPosition--;
    if(currentPosition<0){currentPosition=lenArr-1;}
    beforeChangeImage();
  }

  // 
  setTimeout( function(){
    elemImg.classList.add("animation");  // увеличим картинку
  }, 1000);

  // обработчик клика по элементам
  function clickControl(event){
    switch(event.target.outerText){
      case "Prev":PrevSlide();break;  // по кнопке "Prev"
      case "Next":NextSlide();break;  // по кнопке "Next"
    }
  }