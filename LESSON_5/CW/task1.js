/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var Train = {
    name: "Bullet",
    speed: 0,
    load: 0,
    // Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    move: function ( speed ) {
        if( this.speed == 0 ) {
            this.speed = speed;
        }
        console.log("Поезд ", this.name, " везет ", this.load, "пассажиров со скоростью ", this.speed);        
    },
    // Стоять -> Поезд {name} остановился. Скорость {speed}
    stop: function () {
        this.speed = 0;
        console.log("Поезд ", this.name, " остановился. Скорость ", this.speed);
    },
    // Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
    addPassengers: function ( x ) {
        this.load = this.load + x;
        console.log("Увеличивает кол-во пассажиров на ", x );
    } 
};

console.log("Train", Train);
Train.move( 40 );
Train.stop();
Train.addPassengers(10);
Train.move( 30 );


