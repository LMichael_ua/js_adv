/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white'
  }
 
  function myCall( back ){
    document.body.style.background = back;
    document.body.style.color = this.color;
  }

  myCall.call( colors, 'red' );
  
  //
  function changeColor ( Obj, h1text ) {
    document.body.style.background = this.background;
    document.body.style.color = this.color;

    if (!h1text) {
      h1text = this[1]
    }

    console.log( Obj, h1text );

    // if(h1text == undefined ){
    //   h1text = "I know how binding works in JS";
    // }

    var elH1 = document.createElement("h1");
        elH1.innerText = h1text;
    var docBody = document.body; 
        docBody.appendChild(elH1);
  };

  //
  changeColor(this, 'I know how binding works in JS');

  //
  function callFunc(h1text) {
    changeColor.call(null, this, h1text);
  }

  callFunc("changeColor.call");

  //
  var bindFunc = changeColor.bind(null, this, "changeColor.bind");
  
  bindFunc();

  //
  function applyFunc(arrApply) {
    changeColor.apply(arrApply);
  }

  arrApply = [this, "changeColor.apply"];
  applyFunc(arrApply);

//----------------------------------------------------
  //

