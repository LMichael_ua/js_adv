/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function newDog (name, breed) {
  this.name = name,
  this.breed = breed
}

var objDog = {
  dog: newDog ("Rex", "terrier"),
  status: "",

  changeStatus: function ( status ) {
    this.status = status;
    console.log( this, this.status );
  },

  showAllProperties: function ( obj ) {
    for (key in obj) {
      console.log( key, obj[key] );
    }
  }
  
};

// objDog.name = "Rex";
// objDog.breed = "terrier";

console.log( "Dog", objDog );

objDog.changeStatus( "run" );

objDog.changeStatus( "eat" );

objDog.showAllProperties( objDog );

