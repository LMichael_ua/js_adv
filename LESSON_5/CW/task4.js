/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)


      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

//
function encryptCesar ( word, code ) {
  //console.log( "original:", word, ", code = ", code );

  var arrABCen = word.split("");
  var encryptResult = "";

  arrABCen.forEach(symbol => {
    var tmpCode = symbol.charCodeAt();
    tmpCode = tmpCode + code;
    symbol = String.fromCharCode(tmpCode);
    encryptResult = encryptResult + symbol;
  });

  //console.log( "encrypt resuls:", encryptResult );
  return encryptResult;
};

//
function decryptCesar ( word, code ) {
  //console.log( "original:", word, ", code = ", code );

  var arrABCde = word.split("");
  var decryptResult = "";

  arrABCde.forEach(symbol => {
    var tmpCode = symbol.charCodeAt();
    tmpCode = tmpCode - code;
    symbol = String.fromCharCode(tmpCode);
    decryptResult = decryptResult + symbol;
  });

  //console.log( "decrypt resuls:", decryptResult );
  return decryptResult;
};

var word = "Cesar";



// 1
console.log( "Input word:", word );
var encryptCesar1 = encryptCesar.bind( null, word, 1 );
word = encryptCesar1( );
console.log( "encryptCesar.bind - 1:", word );
//
var decryptCesar1 = decryptCesar.bind( null, word, 1 );
word = decryptCesar1( );
console.log( "decryptCesar.bind - 1:", word ) ;

// 2
console.log( "Input word:", word );
var encryptCesar2 = encryptCesar.bind( null, word, 2 );
word = encryptCesar2( );
console.log( "encryptCesar.bind - 2:", word );
//
var decryptCesar2 = decryptCesar.bind( null, word, 2 );
word = decryptCesar2( );
console.log( "decryptCesar.bind - 2:", word ) ;

// 3
console.log( "Input word:", word );
var encryptCesar3 = encryptCesar.bind( null, word, 3 );
word = encryptCesar3( );
console.log( "encryptCesar.bind - 3:", word );
//
var decryptCesar3 = decryptCesar.bind( null, word, 3 );
word = decryptCesar3( );
console.log( "decryptCesar.bind - 3:", word ) ;

// 4
console.log( "Input word:", word );
var encryptCesar4 = encryptCesar.bind( null, word, 4 );
word = encryptCesar4( );
console.log( "encryptCesar.bind - 4:", word );
//
var decryptCesar4 = decryptCesar.bind( null, word, 4 );
word = decryptCesar4( );
console.log( "decryptCesar.bind - 4:", word ) ;

// 5
console.log( "Input word:", word );
var encryptCesar5 = encryptCesar.bind( null, word, 5 );
word = encryptCesar5( );
console.log( "encryptCesar.bind - 5:", word );
//
var decryptCesar5 = decryptCesar.bind( null, word, 5 );
word = decryptCesar5( );
console.log( "decryptCesar.bind - 5:", word ) ;
