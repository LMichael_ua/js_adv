/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

//
showComment = function ( CommentsArray ) {
  console.log( CommentsArray );

  var elDiv = document.createElement("div");
      elDiv.setAttribute("id", "CommentsFeed");
      document.body.appendChild(elDiv);

  var elDivCommentFeed = document.getElementById("CommentsFeed");

  CommentsArray.forEach(element => {
    var elDivComment = document.createElement("div");
        elDivCommentFeed.appendChild(elDivComment);
    
    var elImg = document.createElement("img");
        elImg.setAttribute("width", 30);
        elImg.setAttribute("height", 30);
        elImg.setAttribute("src", element.avatarUrl);
        elDivComment.appendChild(elImg);

    var elName = document.createElement("input");
        elName.classList.add("label");
        elName.setAttribute("disabled", false);
        elName.setAttribute("size", 4);
        elName.value = element.name;
        elDivComment.appendChild(elName);

    var elInput = document.createElement("input");
        elInput.classList.add("label");
        elInput.setAttribute("disabled", false);
        elInput.value = element.text;
        elDivComment.appendChild(elInput);

    var elLikes = document.createElement("input");
        elLikes.classList.add("label");
        elLikes.setAttribute("disabled", false);
        elLikes.setAttribute("size", 2);
        elLikes.value = element.likes;
        elDivComment.appendChild(elLikes);

      });
}

//
function Comment ( name, text, avatarUrl ) {
  this.name = name,
  this.text = text,
  this.avatarUrl = checkAvatar(avatarUrl),
  this.likes = addLike(this)
}   

//
checkAvatar = function ( avatarUrl ) {
  if ( avatarUrl ) {
    return avatarUrl;
  } else {
    return "img/empty_avatar.jpg";
  }
}

//
addLike = function ( obj ) {
  if ( obj.likes || obj.likes == 0 ) {
    obj.likes++;
  } else {
    return 0;
  } 
}

//
var myComment1 = new Comment ( "LMichael", "Hello", "img/lmichael.jpg" );
console.log( myComment1 );

//
var myComment2 = new Comment ( "Name_2", "-", null );
var myComment3 = new Comment ( "Name_3", "Ha-ha-ha", null );
var myComment4 = new Comment ( "Name_4", "-", null );

var CommentsArray = [myComment1, myComment2, myComment3, myComment4]

//myComment1.likes();
addLike(myComment1);
addLike(myComment3);
addLike(myComment1);

showComment( CommentsArray );

