/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

function dataHandler( json ) {
  console.log( "json", json );
  let elemTable = document.createElement( "table" );
      elemTable.border = "1";
  document.body.appendChild( elemTable );

  let elemHeader = document.createElement( "tr" );
      elemHeader.innerHTML = `
        <tr>
          <td align = center>#</td>
          <td align = center><b>Company</b></td>
          <td align = center><b>Balance</b></td>
          <td align = center><b>Regdate</b></td>
          <td align = center><b>Address</b></td>
        </tr>
      `;
      elemTable.appendChild( elemHeader );

  let i = 0;

  getDate = function(valDate) {
    var tmpDate = valDate.substring( 0, 10 );

//    var tmpTime = valDate.getTime();

    return tmpDate;
  }

  getTextAddress = function(objAdrData) {
    let strAddr = "";
    // let arrAddress = Object.entries(objAdrData);
    // arrAddress.forEach(function(element) {
    //   strAddr = strAddr + element[0] + ":" + element[1] + "</br>";
    // });
    //return strAddr;
    return objAdrData;
  }
  
  json.map( item => {
    i++;
    let elem = document.createElement("tr");
        elem.innerHTML = `
          <tr>
            <td><b>${i}</b></td>
            <td>${item.company}</td>
            <td>${item.balance}</td>
            <td>
              <div><button name="btnRegdate" click="onRegDate">Registered</button></div>
              <div style="display:none">
                ${getDate(item.registered)}
              </div>
            </td>
            <td>
              <div><button name="btnAddress" click="onAddress">Address</button></div>
              <div style="display:none">
                ${getTextAddress(item.address)}
              </div>
            </td>
          </tr>`;
        elemTable.appendChild( elem );
  } )
}

function getTextAddress (objAdrData) {

}

let url = "http://www.json-generator.com/api/json/get/cglVQBYfiq?indent=2"

fetch(url)
.then(
    function(response) {
      if(response.status !== 200){
      
      }

    response.json().then(function(data){
      console.log( data );
      dataHandler( data );
    });
  })
.catch(function(err) {

});

window.addEventListener("load", function(event) {

  window.addEventListener("click", function(event) {
    if(event.target.name == "btnRegdate" ||
       event.target.name == "btnAddress") {
      var btn = event.target.parentElement.parentElement.children[0];
          btn.style.cssText = "display:none";
      var btn = event.target.parentElement.parentElement.children[1];
          btn.style.cssText = "";
    }

  
  });
});
