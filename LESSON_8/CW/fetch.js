/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

  var listData = [];
  var listFriends = [];
  var rndIndex = 0;

  getSubj = function(max) {
    return Math.floor(Math.random() * max);
  }

  myResponseJSON = function() {
    return data;
  }

  getData = function(url) {
    fetch(url)
      .then(
        function(response) {
          if(response.status !== 200) {
            console.log(response.status);
            return;
          }
          response.json().then( function(data) {
            listData = data;
            let rndIndex = getSubj(data.length);
            let rndItem = data[rndIndex];

            console.log("Data:", data);
            console.log("- rndIndex:", rndIndex);
            console.log("- rndItem:", rndItem);

            let url2 = "http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2";
            fetch(url)
            .then(
              function(response) {
                if(response.status !== 200) {
                  console.log(response.status);
                  return;
                }
                response.json().then( function(data) {
                  //console.log("Data2: ", data);
                  let rndItem = data[rndIndex];
                  console.log("Friends:", rndItem.friends);

                })
              })      
          })
      })
  }

  var url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";

  fetch(url)
    .then( getData( url ) )
    .then( getSubj )
    .catch(function(err) {
      console.log("Error:", err);
    })