
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary



  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>

    <button></button>
  </form>


    <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/

onClickToJSON = function() { 

  var vString = "{";

  var colInputs = document.getElementsByTagName("input");
  var arrInputs = Array.from(colInputs);

  arrInputs.forEach(element => {
    if(element.name) {    
      vString = vString + '"' + element.name + '":"' + element.value + '"';
      if(element.name !== "password") {
        vString = vString + ',';
      }
    }
  });   

  vString = vString + "}";
  console.log( "vString: ", vString );

  var elResult = document.getElementById("result");
      elResult.innerText = vString;

}  // onClickToJSON

onClickViewJSON = function() {

  var stringJSON = document.getElementById("stringJSON");
  var myJSON = JSON.parse( stringJSON.value );
  console.log( "myJSON: ", myJSON );

}


var btnToJSON = document.getElementById("btnToJSON");
    btnToJSON.addEventListener("click", onClickToJSON);

var btnViewJSON = document.getElementById("btnViewJSON");
    btnViewJSON.addEventListener("click", onClickViewJSON);
