/*
Скопировать текст из файла RegExp.txt на сайт https://regexr.com/
Написать регулярное выражение которое найдет:
      1. Все слова.
\w+
      2. Все совпадения букв от a до e,
[a-e]
      3. Года, например 2004
\d{4}
      4. Слова обернутые в [квадратныеКавычки]
\[\w+\]
      5. Все форматы файлов .jpg / .png / .gif
\.\w+\s
      6. Все email com.ua
\.(\w+\.\w+)
      7. Все слова написанные с большой буквы
\b([A-Z]\w+)

*/

function readSingleFile(e) {
  if (!e) {
    return;
  }

  var file = e.target.files[0];

  if (!file) {
    return;
  }
 
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    displayContents(contents);

//    alert("Resunt in console log");

    var string = contents;
    var regExp;
    var result;

    // 1. Все слова.
    // = 
    regExp = /\w+/g;
    result = string.match(regExp);
    console.log("1. Все слова:", result);

    // 2. Все совпадения букв от a до e,
    // = [a-e]
    regExp = /[a-e]/g;
    result = string.match(regExp);
    console.log("2. Все совпадения букв от a до e:", result);

    // 3. Года, например 2004
    // = \d{4}
    regExp = /\d{4}/g;
    result = string.match(regExp);
    console.log("3. Года, например 2004:", result);

    // 4. Слова обернутые в [квадратныеКавычки]
    // = \[\w+\]
    regExp = /\[\w+\]/g;
    result = string.match(regExp);
    console.log("4. Слова обернутые в [квадратныеКавычки]:", result);

    // 5. Все форматы файлов .jpg / .png / .gif
    // = \.\w+\s
    regExp = /\.\w+\s/g;
    result = string.match(regExp);
    console.log("5. Все форматы файлов .jpg / .png / .gif:", result);

    // 6. Все email com.ua
    // = \.(\w+\.\w+)
    regExp = /\.(\w+\.\w+)/g;
    result = string.match(regExp);
    console.log("6. Все email com.ua:", result);

    // = (\b\w*)@(\w+\.\w+\b)
    regExp = /(\b\w*)@\w+(.com|.ua)(|\w+\b)/g;
    result = string.match(regExp);
    console.log("6.1. Все email com.ua:", result);

    // = (\b\w*)@(\w+\.\w+\b)
    regExp = /(\b\w*)@(\w+\.\w+\b)/g;
    result = string.match(regExp);
    console.log("6.2. Все email:", result);

    // 7. Все слова написанные с большой буквы
    // = \b([A-Z]\w+)
    regExp = /\b([A-Z]\w+)/g;
    result = string.match(regExp);
    console.log("7. Все слова написанные с большой буквы:", result);
    
  
  };


  reader.readAsText(file);
}

function displayContents(contents) {
  var element = document.getElementById('file-content');
  element.textContent = contents;


}

readSingleFile(event);

document.getElementById('file-input')
  .addEventListener('change', readSingleFile, false);